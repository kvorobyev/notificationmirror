package kvj.notification.mirror.ccs;

public class Main{

    private static final long SENDER_ID = 358576126220L;
    private static final String REDIS_HOST = "127.0.0.1";
    private static final int REDIS_PORT = 6379;

    public static void main(String[] args) {
        CCConnection connection = new CCConnection();
        connection.run(REDIS_HOST, REDIS_PORT, SENDER_ID, args[0]);
    }
}
