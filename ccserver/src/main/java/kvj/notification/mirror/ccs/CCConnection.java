package kvj.notification.mirror.ccs;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.DefaultExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocketFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by kvorobyev on 4/25/15.
 */
public class CCConnection {

    private static final Logger logger = Logger.getLogger("CCConnection");

    private static final String GCM_SERVER = "gcm.googleapis.com";
    private static final int GCM_PORT = 5235;

    private static final String GCM_ELEMENT_NAME = "gcm";
    private static final String GCM_NAMESPACE = "google:mobile:data";
    private JedisPool redisConnection = null;

    private static final class GcmPacketExtension extends DefaultExtensionElement {

        private final String json;

        public GcmPacketExtension(String json) {
            super(GCM_ELEMENT_NAME, GCM_NAMESPACE);
            this.json = json;
        }

        public String getJson() {
            return json;
        }

        @Override
        public String toXML() {
            return String.format("<%s xmlns=\"%s\">%s</%s>\r\n",
                    GCM_ELEMENT_NAME, GCM_NAMESPACE,
                    StringUtils.escapeForXML(json), GCM_ELEMENT_NAME);
        }

        public Stanza toStanza() {
            Message message = new Message();
            message.addExtension(this);
            return message;
        }
    }

    static {

        ProviderManager.addExtensionProvider(GCM_ELEMENT_NAME, GCM_NAMESPACE,
                new ExtensionElementProvider<GcmPacketExtension>() {
                    @Override
                    public GcmPacketExtension parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
                        String json = parser.nextText();
                        return new GcmPacketExtension(json);
                    }
                });
    }

    private XMPPTCPConnection connection;

    protected volatile boolean connectionDraining = false;

    public boolean sendDownstreamMessage(String jsonRequest) throws
            SmackException.NotConnectedException {
        if (!connectionDraining) {
            send(jsonRequest);
            return true;
        }
        logger.warning("Dropping downstream message since the connection is draining");
        return false;
    }

    public String nextMessageId() {
        return "m-" + UUID.randomUUID().toString();
    }

    protected void send(String jsonRequest) throws SmackException.NotConnectedException {
        Stanza request = new GcmPacketExtension(jsonRequest).toStanza();
        connection.sendStanza(request);
    }

    protected void handleUpstreamMessage(Map<String, Object> jsonObject) {
        // PackageName of the application that sent this message.
        String category = (String) jsonObject.get("category");
        String from = (String) jsonObject.get("from");
        @SuppressWarnings("unchecked")
        Map<String, String> payload = (Map<String, String>) jsonObject.get("data");
        Jedis resource = redisConnection.getResource();
        String type = (String) payload.get("data_type");
        String fromID = payload.get("from_id");
        if (null != fromID) {
            resource.set(fromID, from);
        }
        String toID = payload.get("to_id");
//        logger.info("Upstream from: " + from + ", type: " + type + ": " + fromID + " - " + toID+": "+payload);
        if ("data".equals(type)) { // Data - forward
            String[] dests = toID.split(",");
            for (String dest : dests) { // Send new message to every dest
                String to = resource.get(dest.trim());
                if (null == to) { // Failed
                    logger.log(Level.WARNING, "Mapping not found: "+dest);
                    return;
                }
                String message = createJsonMessage(to, nextMessageId(), payload);
                try {
                    send(message);
                    logger.info("Message forwarded: "+dest);
                } catch (SmackException.NotConnectedException e) {
                    logger.log(Level.SEVERE, "Failed to forward", e);
                }
            }
        }
        redisConnection.returnResource(resource);
    }

    protected void handleAckReceipt(Map<String, Object> jsonObject) {
        String messageId = (String) jsonObject.get("message_id");
        String from = (String) jsonObject.get("from");
        logger.log(Level.FINE, "handleAckReceipt() from: " + from + ", messageId: " + messageId);
    }

    protected void handleNackReceipt(Map<String, Object> jsonObject) {
        String messageId = (String) jsonObject.get("message_id");
        String from = (String) jsonObject.get("from");
        logger.log(Level.FINE, "handleNackReceipt() from: " + from + ", messageId: " + messageId);
    }

    protected void handleControlMessage(Map<String, Object> jsonObject) {
        logger.log(Level.FINE, "handleControlMessage(): " + jsonObject);
        String controlType = (String) jsonObject.get("control_type");
        if ("CONNECTION_DRAINING".equals(controlType)) {
            connectionDraining = true;
        } else {
            logger.log(Level.WARNING, "Unrecognized control type: %s. This could happen if new features are " + "added to the CCS protocol.", controlType);
        }
    }

    public static String createJsonMessage(String to, String messageId,
                                           Map<String, String> payload) {
        Map<String, Object> message = new HashMap<String, Object>();
        message.put("to", to);
        message.put("message_id", messageId);
        message.put("data", payload);
        return JSONValue.toJSONString(message);
    }

    protected static String createJsonAck(String to, String messageId) {
        Map<String, Object> message = new HashMap<String, Object>();
        message.put("message_type", "ack");
        message.put("to", to);
        message.put("message_id", messageId);
        return JSONValue.toJSONString(message);
    }

    public void connect(long senderId, String apiKey)
            throws XMPPException, IOException, SmackException {
        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setServiceName(GCM_SERVER)
                .setHost(GCM_SERVER).setPort(GCM_PORT)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
                .setSendPresence(false)
                .setCompressionEnabled(true)
                .setUsernameAndPassword(senderId + "@gcm.googleapis.com", apiKey)
                .setSocketFactory(SSLSocketFactory.getDefault())
                .build();

        connection = new XMPPTCPConnection(config);
        connection.connect();
        Roster roster = Roster.getInstanceFor(connection);
        roster.setRosterLoadedAtLogin(false);

        connection.addConnectionListener(new LoggingConnectionListener());

        // Handle incoming packets
        connection.addSyncStanzaListener(new StanzaListener() {

            @Override
            public void processPacket(Stanza packet) {
                Message incomingMessage = (Message) packet;
                GcmPacketExtension gcmPacket =
                        (GcmPacketExtension) incomingMessage.
                                getExtension(GCM_NAMESPACE);
                String json = gcmPacket.getJson();
                try {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> jsonObject =
                            (Map<String, Object>) JSONValue.
                                    parseWithException(json);

                    // present for "ack"/"nack", null otherwise
                    Object messageType = jsonObject.get("message_type");
                    logger.log(Level.FINE, "Received: " + messageType + ", " + jsonObject);

                    if (messageType == null) {
                        // Normal upstream data message
                        handleUpstreamMessage(jsonObject);

                        // Send ACK to CCS
                        String messageId = (String) jsonObject.get("message_id");
                        String from = (String) jsonObject.get("from");
                        String ack = createJsonAck(from, messageId);
                        send(ack);
                    } else if ("ack".equals(messageType.toString())) {
                        // Process Ack
                        handleAckReceipt(jsonObject);
                    } else if ("nack".equals(messageType.toString())) {
                        // Process Nack
                        handleNackReceipt(jsonObject);
                    } else if ("control".equals(messageType.toString())) {
                        // Process control message
                        handleControlMessage(jsonObject);
                    } else {
                        logger.log(Level.WARNING,
                                "Unrecognized message type (%s)",
                                messageType.toString());
                    }
                } catch (ParseException e) {
                    logger.log(Level.SEVERE, "Error parsing JSON " + json, e);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Failed to process packet", e);
                }
            }
        }, new StanzaTypeFilter(Message.class));

        // Log all outgoing packets
        connection.addPacketInterceptor(new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) {
                logger.log(Level.FINE, "Sent: {0}", packet.toXML());
            }
        }, new StanzaTypeFilter(Message.class));

        connection.login();
        PingManager ping = PingManager.getInstanceFor(connection);
        ping.setPingInterval(60);
        ping.registerPingFailedListener(new PingFailedListener() {
            @Override
            public void pingFailed() {
                logger.info("Ping failed, will re-connect");
                synchronized (lock) {
                    lock.notify();
                }
            }
        });
    }

    private final class LoggingConnectionListener
            implements ConnectionListener {

        @Override
        public void connected(XMPPConnection xmppConnection) {
            logger.info("Connected.");
        }

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            logger.info("Authenticated.");

        }

        @Override
        public void reconnectionSuccessful() {
            logger.info("Reconnecting..");
        }

        @Override
        public void reconnectionFailed(Exception e) {
            logger.log(Level.INFO, "Reconnection failed.. ", e);
        }

        @Override
        public void reconnectingIn(int seconds) {
            logger.log(Level.INFO, "Reconnecting in %d secs", seconds);
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            logger.info("Connection closed on error.");
            synchronized (lock) {
                lock.notify();
            }
        }

        @Override
        public void connectionClosed() {
            logger.info("Connection closed.");
        }
    }

    private final Object lock = new Object();

    public void run(String redisHost, int redisPort, final long senderId, final String apiKey) {
        redisConnection = new JedisPool(redisHost, redisPort);
        try {
            Jedis resource = redisConnection.getResource();
            redisConnection.returnResource(resource);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to connect to Redis", e);
            return;
        }
        while (true) {
            boolean result = runOnce(senderId, apiKey);
            if (!result) { // Failed
                logger.log(Level.INFO, "Exiting");
                return;
            }
            try {
                logger.log(Level.INFO, "Waiting for 5 sec before reconnect");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }


    private boolean runOnce(final long senderId, final String apiKey) {
        try {
            connect(senderId, apiKey);
            synchronized (lock) {
                lock.wait();
            }
            connection.disconnect();
            logger.log(Level.INFO, "Connection closed");
            return true;
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Error waiting", e);
        } catch (SmackException.ConnectionException e) {
            logger.log(Level.SEVERE, "Error connecting - will retry", e);
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to connect:", e);
        }
        return true;
    }

}
