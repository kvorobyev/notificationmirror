package kvj.notification.mirror.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;

public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {

    NotificationController controller = App.controller();
    private Logger logger = Logger.forInstance(this);

    @Override
    public void onMessageReceived(String from, Bundle data) {
        logger.d("New message from:", from, data);
        controller.parseTools().gcmTools().handleMessage(controller, data);
    }
}
