package kvj.notification.mirror.gcm;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.Controller;
import org.kvj.bravo7.util.Listeners;
import org.kvj.bravo7.util.Tasks;

import java.util.UUID;

import kvj.notification.mirror.R;

/**
 * Created by vorobyev on 4/23/15.
 */
public class GcmUtils {

    private String projectID = null;

    public enum MessageType {Data("data"), Registration("register");


        private final String code;

        MessageType(String code) {
            this.code = code;
        }

        public String code() {
            return code;
        }
    };

    private String from = "";

    public interface GcmMessageListener {
        public boolean onMessage(String id, String type, String from, JSONObject data);

        public boolean onAck(String id, boolean ack);
    }

    private Listeners<GcmMessageListener> listeners = new Listeners<>();

    private static final String GOOGLE_SERVER_URL = "https://android.googleapis.com/gcm/notification";
    private Logger logger = Logger.forInstance(this);

    public boolean register(Controller controller, String notificationName) {
        // Do
        this.from = notificationName;
        try {
            this.projectID = controller.context().getString(R.string.gcm_defaultSenderId);
            InstanceID instanceID = InstanceID.getInstance(controller.context());
            String token = instanceID.getToken(projectID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(
                    controller.context());
            if (resultCode != ConnectionResult.SUCCESS) {
                logger.w("Google Play services are not available");
                return false;
            }
            logger.d("Registration done:", token);
            final String sendID = send(controller, MessageType.Registration, null, null);
            return sendID != null; // Sent
        } catch (Exception e) {
            logger.e(e, "Failed to register");
        }
        return false;
    }

    public void sendAsync(final Controller controller, final JSONObject json, final String dest) {
        Tasks.SimpleTask<String> task = new Tasks.SimpleTask<String>() {
            @Override
            protected String doInBackground() {
                return send(controller, MessageType.Data, json, dest);
            }
        };
        task.exec();
    }

    public String send(Controller controller, MessageType type, JSONObject json, String dest) {
        try {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(controller.context());
            String id = String.format("d-%s", UUID.randomUUID().toString());
            Bundle data = new Bundle();
            if (null != json) { // Have data
                data.putString("json_data", json.toString());
            }
            data.putString("id", id);

            data.putString("from_id", from);
            data.putString("data_type", type.code());
            if (null != dest) {
                data.putString("to_id", dest);
            }

            gcm.send(projectID + "@gcm.googleapis.com", id, 86400L, data);
            logger.d("Message sent:", json, dest, id, type);
            return id;
        } catch (Exception e) {
            logger.e(e, "Send failed:");
        }
        return null;
    }

    public void handleMessage(Controller controller, Bundle data) {
        String messageType = data.getString("data_type");
        logger.d("Message type:", messageType, data);
        final String id = data.getString("id");
        if ("send_event".equals(messageType)) { // Ack received
            listeners.emit(new Listeners.ListenerEmitter<GcmMessageListener>() {
                @Override
                public boolean emit(GcmMessageListener listener) {
                    return listener.onAck(id, true);
                }
            });
            return;
        }
        try {
            final String from = data.getString("from_id");
            final String type = data.getString("data_type");

            final JSONObject json;
            if (data.containsKey("json_data")) {
                json = new JSONObject(data.getString("json_data"));
            } else {
                json = null;
            }
            logger.d("Received JSON:", json);
            listeners.emit(new Listeners.ListenerEmitter<GcmMessageListener>() {
                @Override
                public boolean emit(GcmMessageListener listener) {
                    return listener.onMessage(id, type, from, json);
                }
            });
        } catch (JSONException e) {
            logger.e(e, "Invalid JSON");
        }
        return;
    }

    public Listeners<GcmMessageListener> listeners() {
        return listeners;
    }
}
