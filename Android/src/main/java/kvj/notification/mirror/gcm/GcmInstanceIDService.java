package kvj.notification.mirror.gcm;

import com.google.android.gms.iid.InstanceIDListenerService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;

public class GcmInstanceIDService extends InstanceIDListenerService {

    NotificationController controller = App.controller();

    @Override
    public void onTokenRefresh() {
        controller.parseTools().initGCM();
    }
}
