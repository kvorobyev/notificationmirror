package kvj.notification.mirror.ui.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.view.View;

import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.ui.fragment.OneMainPage;

/**
 * Created by kvorobyev on 4/27/15.
 */
public class MainPagesAdapter extends FragmentPagerAdapter {

    private final PagerTitleStrip strip;
    NotificationController controller = App.controller();

    public MainPagesAdapter(FragmentManager fm, PagerTitleStrip strip) {
        super(fm);
        this.strip = strip;
    }

    @Override
    public OneMainPage getItem(int position) {
        return OneMainPage.create(controller.parseTools().receivingFromList().get(position));
    }

    @Override
    public int getCount() {
        return controller.parseTools().receivingFromList().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return controller.parseTools().receivingFromList().get(position).model;
    }

    public void refresh() {
        notifyDataSetChanged();
        strip.setVisibility(getCount() > 0? View.VISIBLE: View.GONE);
    }

}
