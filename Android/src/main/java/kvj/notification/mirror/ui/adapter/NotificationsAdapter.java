package kvj.notification.mirror.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;
import org.kvj.bravo7.util.Tasks;

import java.util.ArrayList;
import java.util.List;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.NotificationTools;

/**
 * Created by kvorobyev on 4/11/15.
 */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private Logger logger = Logger.forInstance(this);

    private NotificationController controller = App.controller();
    private List<NotificationTools.NotificationHolder> data = new ArrayList<>();
    private final String code;

    public NotificationsAdapter(String code) {
        this.code = code;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NotificationTools.NotificationHolder nh = data.get(position);
        logger.d("Render:", position, nh.id());
        holder.notificationFrame.removeAllViews();
        View n = null;
        if (nh.notification().bigContentView != null) { // Have big
            n = nh.notification().bigContentView.apply(holder.notificationFrame.getContext(), holder.notificationFrame);
        } else {
            n = nh.notification().contentView.apply(holder.notificationFrame.getContext(), holder.notificationFrame);
        }
        holder.notificationFrame.addView(n);
        holder.closeButton.setVisibility(NotificationTools.isClosable(nh.notification()) ? View.VISIBLE : View.GONE);
        holder.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logger.d("Ready to close notification", nh.id());
                controller.deleteNotification(nh.id().localID);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void refresh() {
        Tasks.SimpleTask<List<NotificationTools.NotificationHolder>> task = new Tasks.SimpleTask<List<NotificationTools.NotificationHolder>>() {
            @Override
            protected List<NotificationTools.NotificationHolder> doInBackground() {
                return controller.notificationsByCode(code);
            }

            @Override
            protected void onPostExecute(List<NotificationTools.NotificationHolder> notificationHolders) {
                logger.d("Refreshed:", notificationHolders.size());
                data.clear();
                data.addAll(notificationHolders);
                notifyDataSetChanged();
            }
        };
        task.exec();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final FrameLayout notificationFrame;
        private final Button closeButton;
        private final LinearLayout actionButtons;

        public ViewHolder(View itemView) {
            super(itemView);
            notificationFrame = (FrameLayout) itemView.findViewById(R.id.notification_place);
            closeButton = (Button) itemView.findViewById(R.id.notification_close_btn);
            actionButtons = (LinearLayout) itemView.findViewById(R.id.notification_actions);
        }
    }
}
