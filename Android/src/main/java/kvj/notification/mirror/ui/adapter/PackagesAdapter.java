package kvj.notification.mirror.ui.adapter;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.kvj.bravo7.ng.App;
import org.kvj.bravo7.util.Tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.ui.PackageSelector;

/**
 * Created by kvorobyev on 4/21/15.
 */
public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.PackagesViewHolder> {

    private final PackageSelector activity;
    NotificationController controller = App.controller();
    private List<AppInfo> data = new ArrayList<>();

    public PackagesAdapter(PackageSelector activity) {
        this.activity = activity;
    }

    @Override
    public PackagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_packages, parent, false);
        return new PackagesViewHolder(v);
    }

    public void refresh() {
        activity.progressBar().setVisibility(View.VISIBLE);
        data.clear();
        notifyDataSetChanged();
        final PackageManager packageManager = controller.context().getPackageManager();
        Tasks.SimpleTask<List<AppInfo>> task = new Tasks.SimpleTask<List<AppInfo>>() {
            @Override
            protected List<AppInfo> doInBackground() {
                List<ApplicationInfo> apps = packageManager.getInstalledApplications(
                        PackageManager.GET_META_DATA);
                final List<AppInfo> infos = new ArrayList<>();
                for (ApplicationInfo info : apps) {
                    AppInfo appInfo = new AppInfo();
                    appInfo.packageName = info.packageName;
                    appInfo.name = info.packageName;
                    // appInfo.selected = packages.contains(info.packageName);
                    CharSequence desc = info.loadLabel(packageManager);
                    if (null != desc) {
                        appInfo.name = desc.toString();
                    }
                    appInfo.logo = info.loadIcon(packageManager);
                    infos.add(appInfo);
                }
                Collections.sort(infos, new Comparator<AppInfo>() {

                    @Override
                    public int compare(AppInfo lhs, AppInfo rhs) {
                        return lhs.name.compareToIgnoreCase(rhs.name);
                    }
                });
                return infos;
            }

            @Override
            protected void onPostExecute(List<AppInfo> applicationInfos) {
                data.clear();
                data.addAll(applicationInfos);
                activity.progressBar().setVisibility(View.GONE);
                notifyDataSetChanged();
            }
        };
        task.exec();
    }

    @Override
    public void onBindViewHolder(PackagesViewHolder holder, int position) {
        final AppInfo info = data.get(position);
        holder.icon.setImageDrawable(info.logo);
        holder.label.setText(info.name);
        boolean selected = controller.packageSelected(info.packageName);
        holder.root.setBackgroundColor(controller.context().getResources().getColor(
                selected? android.R.color.background_light: android.R.color.background_dark));
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.selectPackage(info.packageName, !controller.packageSelected(info.packageName));
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class AppInfo {

        String name;
        Drawable logo;
        String packageName;
        boolean selected = false;
    }

    public static class PackagesViewHolder extends RecyclerView.ViewHolder {

        private final ImageView icon;
        private final TextView label;
        private final ViewGroup root;

        public PackagesViewHolder(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            icon = (ImageView) itemView.findViewById(R.id.packages_item_icon);
            label = (TextView) itemView.findViewById(R.id.packages_item_label);
        }
    }
}
