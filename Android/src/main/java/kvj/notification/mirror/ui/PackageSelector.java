package kvj.notification.mirror.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.ui.adapter.PackagesAdapter;

/**
 * Created by kvorobyev on 4/20/15.
 */
public class PackageSelector extends AppCompatActivity {
    private NotificationController controller = App.controller();
    private Logger logger = Logger.forInstance(this);
    private CheckBox modeSwitch = null;
    private ProgressBar progressBar = null;
    private RecyclerView list = null;
    private ViewGroup switchBox = null;
    private ViewGroup packagesPanel = null;
    private PackagesAdapter listAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages);
        modeSwitch = (CheckBox) findViewById(R.id.packages_switch);
        switchBox = (ViewGroup) findViewById(R.id.packages_switch_box);
        packagesPanel = (ViewGroup) findViewById(R.id.packages_panel);
        progressBar = (ProgressBar) findViewById(R.id.packages_progress);
        list = (RecyclerView) findViewById(R.id.packages_list);
        list.setLayoutManager(new GridLayoutManager(this, 3));
        listAdapter = new PackagesAdapter(this);
        list.setAdapter(listAdapter);
        modeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleSwitch(isChecked);
            }
        });
    }

    private void toggleSwitch(boolean on) {
        logger.d("toggleSwitch", on);
        modeSwitch.setChecked(on);
        controller.allPackages(on);
        if (on) { // Hide panel
            packagesPanel.setVisibility(View.GONE);
            switchBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        } else {
            packagesPanel.setVisibility(View.VISIBLE);
            switchBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0));
            listAdapter.refresh();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                logger.d("List:", list.getWidth(), list.getHeight(), list.getMeasuredWidth(), list.getMeasuredHeight());
                toggleSwitch(controller.allPackages());
            }
        });
    }

    public ProgressBar progressBar() {
        return progressBar;
    }
}
