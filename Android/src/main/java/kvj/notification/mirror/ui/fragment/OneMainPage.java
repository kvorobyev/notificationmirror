package kvj.notification.mirror.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.ParseTools;
import kvj.notification.mirror.ui.adapter.NotificationsAdapter;

/**
 * Created by kvorobyev on 4/27/15.
 */
public class OneMainPage extends Fragment implements NotificationController.RemoteNotificationsListener {

    private ParseTools.PairingInfo info = null;
    private RecyclerView listView = null;
    private NotificationsAdapter listAdapter = null;
    private NotificationController controller = App.controller();

    public static OneMainPage create(ParseTools.PairingInfo info) {
        OneMainPage item = new OneMainPage();
        item.init(info);
        return item;
    }

    private void init(ParseTools.PairingInfo info) {
        this.info = info;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == info) {
            return null;
        }
        View view = inflater.inflate(R.layout.pane_notifications, container, false);
        listView = (RecyclerView) view.findViewById(R.id.main_list);
        listView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        listAdapter = new NotificationsAdapter(info.code);
        listView.setAdapter(listAdapter);
        return view;
    }

    public void refresh() {
        if (null != listAdapter) {
            listAdapter.refresh();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        controller.notificationsListeners().add(this);
    }

    @Override
    public void onPause() {
        controller.notificationsListeners().remove(this);
        super.onPause();
    }

    @Override
    public void onNotificationsChange() {
        refresh();
    }
}
