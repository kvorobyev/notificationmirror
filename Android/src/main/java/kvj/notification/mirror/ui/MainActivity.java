package kvj.notification.mirror.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONObject;
import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.ParseTools;
import kvj.notification.mirror.ui.adapter.MainPagesAdapter;

public class MainActivity extends AppCompatActivity implements
        NotificationController.RemoteNotificationsListener, ParseTools.ParseMessageListener {

    private NotificationController controller = App.controller();
    private Toolbar toolbar = null;
    private View notificationsPane = null;
    private View gcmPane = null;
    private ViewPager viewPager = null;
    private MainPagesAdapter pagesAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.main_pager);
        pagesAdapter = new MainPagesAdapter(getSupportFragmentManager(), (PagerTitleStrip) findViewById(R.id.main_page_strip));
        viewPager.setAdapter(pagesAdapter);
        notificationsPane = findViewById(R.id.main_notification_notifications);
        gcmPane = findViewById(R.id.main_notification_gcm);
        findViewById(R.id.main_notification_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        });
        findViewById(R.id.main_gcm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.parseTools().initGCM();
            }
        });
        if (controller.parseTools().isEmpty()
            && controller.parseTools().initDone()) { // Not configured
            startActivity(new Intent(this, PairingConfig.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(this, PairingConfig.class));
                return true;
            case R.id.action_packages:
                startActivity(new Intent(this, PackageSelector.class));
                return true;
            case R.id.action_refresh:
                int pageIndex = viewPager.getCurrentItem();
                if (pageIndex >= 0
                        && pageIndex < controller.parseTools().receivingFromList().size()) {
                    controller.refresh(controller.parseTools().receivingFromList().get(pageIndex).code);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        controller.notificationsListeners().add(this);
        controller.parseTools().listeners().add(this);
    }

    @Override
    protected void onPause() {
        controller.notificationsListeners().remove(this);
        controller.parseTools().listeners().remove(this);
        super.onPause();
    }

    @Override
    public void onNotificationsChange() {
        notificationsPane.setVisibility(controller.notificationListenerSet()? View.GONE: View.VISIBLE);
    }

    @Override
    public void onConfigChanged() {
        gcmPane.setVisibility(controller.parseTools().initDone()? View.GONE: View.VISIBLE);
        pagesAdapter.refresh();
    }

    @Override
    public boolean onMessage(String from, String type, JSONObject message) {
        return true;
    }
}
