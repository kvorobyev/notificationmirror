package kvj.notification.mirror.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.kvj.bravo7.adapter.LayoutsPagerAdapter;
import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.ParseTools;
import kvj.notification.mirror.ui.adapter.PairingsAdapter;

/**
 * Created by kvorobyev on 4/11/15.
 */
public class PairingConfig extends AppCompatActivity implements ParseTools.ParseMessageListener {

    private LayoutsPagerAdapter panesAdapter = null;
    private ViewPager pager = null;
    private RecyclerView sendingList, receivingList;
    private PairingsAdapter sendingListAdapter = null, receivingListAdapter = null;
    private NotificationController controller = App.controller();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing);
        panesAdapter = new LayoutsPagerAdapter(getSupportFragmentManager(), R.layout.pane_sending_to, R.layout.pane_receiving_to) {
            @Override
            protected void customize(View view, int position, int layoutID) {
                switch (layoutID) {
                    case R.layout.pane_sending_to:
                        sendingList = (RecyclerView) view.findViewById(R.id.pairing_sending_list);
                        sendingList.setLayoutManager(new LinearLayoutManager(PairingConfig.this));
                        sendingListAdapter = new PairingsAdapter(controller.parseTools().sendingToList()) {
                            @Override
                            protected void onRemove(int position) {
                                controller.parseTools().removeSender(position);
                            }
                        };
                        sendingList.setAdapter(sendingListAdapter);
                        setupInput(view);
                        break;
                    case R.layout.pane_receiving_to:
                        receivingList = (RecyclerView) view.findViewById(R.id.pairing_receiving_list);
                        receivingList.setLayoutManager(new LinearLayoutManager(PairingConfig.this));
                        receivingListAdapter = new PairingsAdapter(controller.parseTools().receivingFromList()) {
                            @Override
                            protected void onRemove(int position) {
                                controller.parseTools().removeReceiver(position);
                            }
                        };
                        receivingList.setAdapter(receivingListAdapter);
                        break;
                }
            }

            @Override
            protected CharSequence pageTitle(int position, int layoutID) {
                switch (layoutID) {
                    case R.layout.pane_sending_to:
                        return "Sending To";
                    case R.layout.pane_receiving_to:
                        return "Receiving From";
                }
                return null;
            }
        };
        pager = (ViewPager) findViewById(R.id.pairing_pager);
        pager.setAdapter(panesAdapter);
        TextView myCode = (TextView) findViewById(R.id.pairing_my_code);
        myCode.setText(ParseTools.formatCode(controller.parseTools().code()));
    }

    private void setupInput(View view) {
        final EditText input = (EditText) view.findViewById(R.id.pairing_sending_input);
        final Button pair = (Button) view.findViewById(R.id.pairing_sending_button);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String formatted = ParseTools.formatCode(s.toString());
                boolean inputOK = false;
                if (!TextUtils.isEmpty(ParseTools.normalizeCode(s.toString()))) { // Full OK
                    inputOK = true;
                }
                pair.setEnabled(inputOK);
            }
        });
        pair.setEnabled(false);
        pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.parseTools().sendPairingRequest(ParseTools.normalizeCode(input.getText().toString()));
                Toast.makeText(PairingConfig.this, "Pairing Request Sent", Toast.LENGTH_LONG).show();
                input.setText("");
            }
        });
    }

    @Override
    protected void onResume() {
        controller.parseTools().listeners().add(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        controller.parseTools().listeners().remove(this);
        super.onPause();
    }

    @Override
    public void onConfigChanged() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != receivingListAdapter) {
                    receivingListAdapter.refresh();
                }
                if (null != sendingListAdapter) {
                    sendingListAdapter.refresh();
                }
            }
        });
    }

    @Override
    public boolean onMessage(final String from, String type, JSONObject message) {
        final String model = message.optString("model", "<Unknown>");
        if (ParseTools.MESSAGE_PAIR.equals(type)) { // Show dialog
            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage(String.format("Accept pairing request?\nID: %s\nModel: %s",
                                           ParseTools.formatCode(from), model));
            alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    controller.parseTools().sendPairingResponse(from);
                    controller.parseTools().addReceiver(from, model);
                    Toast.makeText(PairingConfig.this, "Accepted", Toast.LENGTH_LONG).show();
                }
            }).setNegativeButton("Reject", null);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alert.show();
                }
            });
            return false; // Stop
        }
        if (ParseTools.MESSAGE_PAIR_ACCEPT.equals(type)) { // Show toast
            controller.parseTools().addSender(from, model);
            Toast.makeText(PairingConfig.this, "Accepted", Toast.LENGTH_LONG).show();
        }
        return true;
    }
}
