package kvj.notification.mirror.ui.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.impl.ParseTools;

/**
 * Created by kvorobyev on 4/11/15.
 */
public class PairingsAdapter extends RecyclerView.Adapter<PairingsAdapter.ViewHolder>{

    private final List<ParseTools.PairingInfo> array;

    public PairingsAdapter(List<ParseTools.PairingInfo> array) {
        this.array = array;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pairing, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ParseTools.PairingInfo info = array.get(position);
        holder.codeText.setText(ParseTools.formatCode(info.code));
        holder.modelText.setText(info.model);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(holder, position);
            }
        });
    }

    private void removeItem(ViewHolder holder, final int position) {
        ParseTools.PairingInfo info = array.get(position);
        AlertDialog.Builder alert = new AlertDialog.Builder(holder.button.getContext());
        alert.setMessage(String.format("Really want to remove?\nID: %s",
                ParseTools.formatCode(info.code)));
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onRemove(position);
            }
        }).setNegativeButton("No", null);
        alert.show();
    }

    protected void onRemove(int position) {
    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView codeText;
        private final TextView modelText;
        private final Button button;

        public ViewHolder(View itemView) {
            super(itemView);
            codeText = (TextView) itemView.findViewById(R.id.pairing_item_code);
            modelText = (TextView) itemView.findViewById(R.id.pairing_item_model);
            button = (Button) itemView.findViewById(R.id.pairing_item_button);
        }
    }
}
