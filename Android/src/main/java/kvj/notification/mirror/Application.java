package kvj.notification.mirror;

import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;

/**
 * Created by kvorobyev on 4/8/15.
 */
public class Application extends App<NotificationController> {

    @Override
    protected NotificationController create() {
        return new NotificationController(this);
    }

    @Override
    protected void init() {
    }
}
