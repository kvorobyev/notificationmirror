package kvj.notification.mirror.app.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;

public class ActionReceiver extends BroadcastReceiver {

    public static final String EXTRA_ID = "notification_id";
    public static final String EXTRA_INDEX = "action_index";

    NotificationController controller = App.controller();

    public ActionReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra(EXTRA_ID, -1);
        int index = intent.getIntExtra(EXTRA_INDEX, -1);
        controller.handleAction(id, index);
    }
}
