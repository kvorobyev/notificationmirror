package kvj.notification.mirror.app.svc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;

import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.NotificationTools;

public class DeleteReceiver extends BroadcastReceiver {
    Logger logger = Logger.forInstance(this);
    NotificationController controller = App.controller();

    @Override
    public void onReceive(Context context, Intent intent) {
        logger.d("Notification removed", intent);
        int localID = intent.getIntExtra(NotificationTools.EXTRA_LOCAL_ID, -1);
        if (localID > 0) { // Looks valid
            controller.deleteNotification(localID);
        }
    }
}
