package kvj.notification.mirror.app.impl;

import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.util.Listeners;
import org.kvj.bravo7.util.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.gcm.GcmUtils;

/**
 * Created by kvorobyev on 4/11/15.
 */
public class ParseTools implements GcmUtils.GcmMessageListener {

    @Override
    public boolean onMessage(String id, String type, String from, JSONObject data) {
        incoming(data);
        return true;
    }

    @Override
    public boolean onAck(String id, boolean ack) {
        return true;
    }

    public boolean isEmpty() {
        return receivingFromList().isEmpty() && sendingToList().isEmpty();
    }

    public static class PairingInfo {

        public String code;
        public String model = "<Unknown>";
    }

    public static final String MESSAGE_ACTION = "action";
    public static final String MESSAGE_PAIR = "pair";
    public static final String MESSAGE_REFRESH = "refresh";
    public static final String MESSAGE_PAIR_ACCEPT = "pair_accept";
    public static final String MESSAGE_NOTIFY = "notify";
    public static final String MESSAGE_REMOVE = "remove";
    public static final String MESSAGE_REMOVED = "removed"; // When receiver clears notification

    private List<PairingInfo> sendingTo = new ArrayList<>();
    private List<PairingInfo> receivingFrom = new ArrayList<>();

    public interface ParseMessageListener {
        public void onConfigChanged();

        public boolean onMessage(String from, String type, JSONObject message);
    }

    private final NotificationController controller;
    private Logger logger = Logger.forInstance(this);
    private boolean initDone = false;
    private Listeners<ParseMessageListener> listeners = new Listeners<ParseMessageListener>() {
        @Override
        protected void onAdd(ParseMessageListener listener) {
            listener.onConfigChanged();
        }
    };
    private final String code;

    private GcmUtils gcmUtils = new GcmUtils();

    public ParseTools(NotificationController controller) {
        this.controller = controller;
        String c = controller.settings().settingsString(R.string.settings_code, "");
        if (TextUtils.isEmpty(c)) { // Need new
            Random r = new Random();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 12; i++) {
                builder.append(Integer.toString(r.nextInt(10)));
            }
            c = builder.toString();
            controller.settings().stringSettings(R.string.settings_code, c);
        }
        array2list(receivingFrom,
                   controller.settings().settingsArray(R.string.settings_receiving, new JSONArray()));
        array2list(sendingTo,
                   controller.settings().settingsArray(R.string.settings_sending, new JSONArray()));
        code = c;
        gcmUtils.listeners().add(this);
        initGCM();
    }

    public GcmUtils gcmTools() {
        return gcmUtils;
    }

    public void initGCM() {
        Tasks.SimpleTask<Boolean> task = new Tasks.SimpleTask<Boolean>() {
            @Override
            protected Boolean doInBackground() {
                boolean ok = gcmUtils.register(controller, code());
                logger.d("GCM init:", ok);
                return ok;

            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                initDone = aBoolean;
                notifyChanged();
                Toast toast = Toast.makeText(controller.context(),
                        initDone ? R.string.message_registration_ok : R.string.message_registration_failure,
                        Toast.LENGTH_LONG);
                toast.show();
            }
        };
        task.exec();
    }

    public String code() {
        return code;
    }

    private static Pattern codeFormat = Pattern.compile("^(\\d{4})(\\-?(\\d{4}))?(\\-?(\\d{4}))?$");

    public static String formatCode(String c) {
        Matcher m = codeFormat.matcher(c);
        if (!m.find()) { // Invalid input
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(m.group(1));
        if (!TextUtils.isEmpty(m.group(2))) {
            sb.append("-");
            sb.append(m.group(3));
        }
        if (!TextUtils.isEmpty(m.group(4))) {
            sb.append("-");
            sb.append(m.group(5));
        }
        return sb.toString();
    }

    public static String normalizeCode(String c) {
        Matcher m = codeFormat.matcher(c);
        if (!m.find() || TextUtils.isEmpty(m.group(4))) { // Invalid input
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(m.group(1));
        sb.append(m.group(3));
        sb.append(m.group(4));
        return sb.toString();
    }

    public Listeners<ParseMessageListener> listeners() {
        return listeners;
    }

    private PairingInfo fromJson(JSONObject json) {
        PairingInfo info = new PairingInfo();
        info.code = json.optString("code");
        info.model = json.optString("model");
        return info;
    }

    private JSONObject toJson(PairingInfo info) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("code", info.code);
        obj.put("model", info.model);
        return obj;
    }

    private void array2list(List<PairingInfo> list, JSONArray arr) {
        list.clear();
        for (int i = 0; i < arr.length(); i++) { // $COMMENT
            list.add(fromJson(arr.optJSONObject(i)));
        }
    }

    private JSONArray list2array(List<PairingInfo> list, JSONArray arr) throws JSONException {
        for (PairingInfo pairingInfo : list) { // $COMMENT
            arr.put(toJson(pairingInfo));
        }
        return arr;
    }

    public void sendPairingRequest(String s) {
        JSONObject request = new JSONObject();
        try {
            request.put("model", getDeviceName());
            send(s, MESSAGE_PAIR, request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean send(String dest, String type, JSONObject request) {
        if (!initDone) { // Not yet
            return false;
        }
        try {
            request.put("from", code());
            request.put("type", type);
            gcmTools().sendAsync(controller, request, dest);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public void incoming(final JSONObject json) {
        logger.i("Incoming JSON:", json);
        final String from = json.optString("from");
        String codeFrom = normalizeCode(from);
        if (TextUtils.isEmpty(codeFrom)) { // Invalid
            return;
        }
        final String type = json.optString("type");
        listeners.emit(new Listeners.ListenerEmitter<ParseMessageListener>() {
            @Override
            public boolean emit(ParseMessageListener listener) {
                return listener.onMessage(from, type, json);
            }
        });
    }

    private String codesByComma(List<PairingInfo> list) {
        StringBuilder sb = new StringBuilder();
        synchronized (list) {
            for (int i = 0; i < list.size(); i++) { // Join
                if (i>0) {
                    sb.append(',');
                }
                sb.append(list.get(i).code);
            }
        }
        return sb.toString();
    }

    public void sendNotification(JSONObject json, boolean removed) {
        if (sendingTo.isEmpty()) {
            return; // No recipients
        }
        send(codesByComma(sendingTo), removed? MESSAGE_REMOVE: MESSAGE_NOTIFY, json);
    }

    public void sendAction(NotificationTools.NotificationID id, int index) {
        if (!allowedRecipient(id.code)) { // Just in case
            logger.w("Recipient not allowed", id);
            return;
        }
        JSONObject json = controller.notificationTools().serializeSimple(id);
        try {
            json.put("index", index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        send(id.code, MESSAGE_ACTION, json);
    }

    public void sendDeleted(NotificationTools.NotificationID id) {
        // Send simple
        if (!allowedRecipient(id.code)) { // Just in case
            logger.w("Recipient not allowed", id);
            return;
        }
        JSONObject json = controller.notificationTools().serializeSimple(id);
        send(id.code, MESSAGE_REMOVED, json);
    }

    public boolean allowedSender(String code) {
        synchronized (sendingTo) {
            for (PairingInfo pairingInfo : sendingTo) {
                if (pairingInfo.code.equals(code)) { // OK
                    return true;
                }
            }
        }
        return false;
    }

    public boolean allowedRecipient(String code) {
        synchronized (receivingFrom) {
            for (PairingInfo pairingInfo : receivingFrom) { // $COMMENT
                if (pairingInfo.code.equals(code)) { // OK
                    return true;
                }
            }
        }
        return false;
    }

    public boolean sendRefresh(String code) {
        if (!allowedRecipient(code)) {
            return false;
        }
        return send(code, MESSAGE_REFRESH, new JSONObject());
    }

    public void sendPairingResponse(String from) {
        JSONObject request = new JSONObject();
        try {
            request.put("model", getDeviceName());
            send(from, MESSAGE_PAIR_ACCEPT, request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private PairingInfo describePairing(String from, String model) {
        PairingInfo info = new PairingInfo();
        info.code = from;
        info.model = model;
        return info;
    }

    public void addReceiver(String from, String model) {
        receivingFrom.add(describePairing(from, model));
        save();
    }

    public void addSender(String from, String model) {
        sendingTo.add(describePairing(from, model));
        save();
    }

    public void removeReceiver(int position) {
        if (position < receivingFrom.size()) { //
            receivingFrom.remove(position);
            save();
        }
    }

    public void removeSender(int position) {
        if (position < sendingTo.size()) { //
            sendingTo.remove(position);
            save();
        }
    }

    private void notifyChanged() {
        listeners.emit(new Listeners.ListenerEmitter<ParseMessageListener>() {
            @Override
            public boolean emit(ParseMessageListener listener) {
                listener.onConfigChanged();
                return true;
            }
        });
    }

    private void save() {
        try {
            controller.settings().
                arraySettings(R.string.settings_sending, list2array(sendingTo, new JSONArray()));
            controller.settings().
                arraySettings(R.string.settings_receiving, list2array(receivingFrom, new JSONArray()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notifyChanged();
    }

    public List<PairingInfo> sendingToList() {
        return sendingTo;
    }

    public List<PairingInfo> receivingFromList() {
        return receivingFrom;
    }

    public boolean initDone() {
        return initDone;
    }
}
