package kvj.notification.mirror.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.kvj.bravo7.ng.Controller;
import org.kvj.bravo7.util.Compat;
import org.kvj.bravo7.util.Listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.CRC32;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.impl.NotificationTools;
import kvj.notification.mirror.app.impl.ParseTools;
import kvj.notification.mirror.app.svc.NotificationListener2;

/**
 * Created by kvorobyev on 4/8/15.
 */
public class NotificationController extends Controller  implements ParseTools.ParseMessageListener {

    private final ParseTools parseTools;

    public interface RemoteNotificationsListener {
        public void onNotificationsChange();
    }

    private Listeners<RemoteNotificationsListener> notificationsListeners = new Listeners<RemoteNotificationsListener>() {
        @Override
        protected void onAdd(RemoteNotificationsListener listener) {
            listener.onNotificationsChange();
        }
    };

    private final NotificationTools notificationTools;
    private NotificationListener2 notificationListener = null;
    private List<StatusBarNotification> notifications = new ArrayList<>();
    private Map<String, Long> crcMap = new HashMap<>();

    private int lastID = 0;
    private Map<String, NotificationTools.NotificationID> notificationIDs = new HashMap<>();

    private boolean allPackages = true;
    private Set<String> packages = new HashSet<>();

    public NotificationController(Context context) {
        super(context, "Mirror: ");
        allPackages = settings().settingsBoolean(R.string.settings_all_packages, true);
        JSONArray array = settings().settingsArray(R.string.settings_packages, new JSONArray());
        for (int i = 0; i < array.length(); i++) { // $COMMENT
            packages.add(array.optString(i));
        }
        notificationTools = new NotificationTools(context);
        parseTools = new ParseTools(this);
        parseTools.listeners().add(this);
        logger.d("Application started");
    }

    public NotificationTools notificationTools() {
        return notificationTools;
    }

    public NotificationTools.NotificationID idForNotification(final NotificationTools.NotificationID by, Notification notification) {
        String key = notificationTools.keyFromNotification(by);
        synchronized (notificationIDs) {
            NotificationTools.NotificationID id = notificationIDs.get(key);
            if (null != id) { // OK
                id.deleted = false;
                id.notification = notification;
                return id;
            }
            by.deleted = false;
            by.notification = notification;
            by.localID = ++lastID;
            notificationIDs.put(key, by);
            return by;
        }
    }

    public synchronized NotificationTools.NotificationID findIdForNotification(NotificationTools.NotificationID id) {
        return notificationIDs.get(notificationTools.keyFromNotification(id));
    }

    public NotificationTools.NotificationID notificationByLocalID(int id) {
        synchronized (notificationIDs) {
            for (NotificationTools.NotificationID notificationID : notificationIDs.values()) {
                if (notificationID.localID == id) { // Found
                    return notificationID;
                }
            }
        }
        return null;
    }

    public void notificationListener(NotificationListener2 notificationListener, StatusBarNotification[] notifications) {
        boolean set = null == this.notificationListener;
        this.notificationListener = notificationListener;
        synchronized (this.notifications) {
            this.notifications.clear();
            Collections.addAll(this.notifications, notifications);
        }
        remoteNotificationsChanged();
        if (set) { // Refresh all
            synchronized (this.notifications) {
                for (StatusBarNotification sbn : this.notifications) {
                    if (!sbn.getPackageName().equals(context.getPackageName())) { // Own notification - translate and close
                        sendNotification(sbn, false);
                    }
                }
            }
        }
    }

    public boolean notificationListenerSet() {
        return notificationListener != null;
    }

    public void remoteNotificationsChanged() {
        notificationsListeners.emit(new Listeners.ListenerEmitter<RemoteNotificationsListener>() {
            @Override
            public boolean emit(RemoteNotificationsListener listener) {
                listener.onNotificationsChange();
                return true;
            }
        });
    }

    public Listeners<RemoteNotificationsListener> notificationsListeners() {
        return notificationsListeners;
    }

    public List<NotificationTools.NotificationHolder> notificationsByCode(String code) {
        List<NotificationTools.NotificationHolder> result = new ArrayList<>();
        Map<Integer, NotificationTools.NotificationID> ids = new HashMap<>();
        for (NotificationTools.NotificationID id : notificationIDs.values()) {
            if (!id.deleted && (TextUtils.isEmpty(code) || id.code.equals(code))) { // Only kind of active
                ids.put(id.localID, id);
            }
        }
        logger.d("Will get notifications:", notificationListener, notificationIDs.size(), code, ids.size());
        if (null == notificationListener) {
            // No StatusBarNotification mode
            for (NotificationTools.NotificationID id : ids.values()) {
                result.add(new NotificationTools.NotificationHolder(id, id.notification));
            }
            return result;
        }
        for (StatusBarNotification sbn : notifications) { // Match selected IDs with real notifications
            if (!sbn.getPackageName().equals(context.getPackageName())) { // Own
                continue;
            }
            NotificationTools.NotificationID id = ids.get(sbn.getId());
            if (null == id) { // Skip
                continue;
            }
            result.add(new NotificationTools.NotificationHolder(id, sbn.getNotification()));
        }
        return result;
    }

    public void refresh(String code) {
        // Clear all and send refresh event
        List<NotificationTools.NotificationHolder> list = notificationsByCode(code);
        for (NotificationTools.NotificationHolder nh : list) { // Clear
            notificationTools.clear(nh.id());
        }
        remoteNotificationsChanged();
        parseTools.sendRefresh(code);
    }

    public ParseTools parseTools() {
        return parseTools;
    }

    public boolean sendNotification(StatusBarNotification sbn, boolean removed) {
        if (!packageSelected(sbn.getPackageName())) { // Ignore
            return false;
        }
        JSONObject json = notificationTools.serialize(sbn, removed);
        if (sameCRC(json, sbn, removed)) { // Do not need to send
            return false;
        }
        parseTools.sendNotification(json, removed);
        return true;
    }

    private boolean sameCRC(JSONObject origJson, StatusBarNotification sbn, boolean removed) {
        try {
            JSONObject json = new JSONObject(origJson.toString());
            json.remove("when");
            String id = notificationTools.idFromNotification(sbn).toString();
            if (removed) {
                crcMap.remove(id);
                return false;
            }
            CRC32 crc = new CRC32();
            crc.update(json.toString().getBytes("utf-8"));
            long value = crc.getValue();
            Long saved = crcMap.get(id);
            if (null != saved && saved == value) { // Do not need to send
                return true;
            }
            crcMap.put(id, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onConfigChanged() {
    }

    @Override
    public boolean onMessage(String from, String type, final JSONObject message) {
        logger.d("onMessage", type, from);
        boolean isUpdate = ParseTools.MESSAGE_NOTIFY.equals(type);
        boolean isRemove = ParseTools.MESSAGE_REMOVE.equals(type);
        if (isUpdate || isRemove) { // Message tools
            if (!parseTools.allowedRecipient(from)) { // Invalid
                return false; // Stop
            }
            NotificationTools.NotificationID id = notificationTools.idFromJSON(message, from);
            if (isRemove) { // Have to remove
                NotificationTools.NotificationID existing = findIdForNotification(id);
                notificationTools.clear(existing);
            }
            if (isUpdate) { // Go
                NotificationCompat.Builder nb = new NotificationCompat.Builder(context);
                nb.setSmallIcon(R.drawable.ic_stat_full);
                final NotificationTools.NotificationID newId = idForNotification(id, null);
                final Notification n = notificationTools.deserializeNotification(nb, message, newId.localID);
                newId.notification = n;
                n.deleteIntent = notificationTools.createDeleteIntent(newId);
                notificationTools.show(n, newId);
            }
            remoteNotificationsChanged();
            return false;
        }
        if (ParseTools.MESSAGE_REMOVED.equals(type)) { // Removed by other side
            if (!parseTools.allowedSender(from) || notificationListener == null) { // Invalid
                logger.w("Invalid sender or no listener", from, notificationListener);
                return false; // Stop
            }
            NotificationTools.NotificationID id = notificationTools.idFromJSON(message, from);
            logger.d("Will remove", id, notificationListener.find(id));
            notificationListener.cancel(notificationListener.find(id));
        }
        if (ParseTools.MESSAGE_ACTION.equals(type)) { // Removed by other side
            if (!parseTools.allowedSender(from) || notificationListener == null) { // Invalid
                logger.w("Invalid sender or no listener", from, notificationListener);
                return false; // Stop
            }
            final int index = message.optInt("index", -1);
            NotificationTools.NotificationID id = notificationTools.idFromJSON(message, from);
            final StatusBarNotification sbn = notificationListener.find(id);
            logger.d("Will trigger action", id, sbn, index);
            Compat.levelAware(19, new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {
                    if (null == sbn.getNotification().actions
                            || index < 0
                            || index >= sbn.getNotification().actions.length) {
                        logger.w("Invalid action:", index, sbn.getNotification().actions);
                        return;
                    }
                    try {
                        sbn.getNotification().actions[index].actionIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        logger.e(e, "Failed to execute action");
                    }
                }
            });
        }
        if (ParseTools.MESSAGE_REFRESH.equals(type)) { // Send all again
            if (!parseTools.allowedSender(from) || notificationListener == null) { // Invalid
                logger.w("Invalid sender or no listener", from, notificationListener);
                return false; // Stop
            }
            synchronized (this.notifications) {
                crcMap.clear(); // Just send all
                for (StatusBarNotification sbn : this.notifications) {
                    if (!sbn.getPackageName().equals(context.getPackageName()) &&
                            packageSelected(sbn.getPackageName())) {
                        // Other notification
                        sendNotification(sbn, false);
                    }
                }
            }
        }
        return true;
    }

    public void deleteNotification(int localID) {
        NotificationTools.NotificationID id = notificationByLocalID(localID);
        if (id == null || id.deleted) { // Already done
            remoteNotificationsChanged(); // Update UI
            return;
        }
        // Otherwise - report
        notificationTools.clear(id);
        remoteNotificationsChanged(); // Update UI
        parseTools.sendDeleted(id);
    }

    public boolean allPackages() {
        return allPackages;
    }

    public void allPackages(boolean allPackages) {
        if (this.allPackages != allPackages) { // Changed
            this.allPackages = allPackages;
            settings().booleanSettings(R.string.settings_all_packages, allPackages);
        }
    }

    public void selectPackage(String packageName, boolean selected) {
        synchronized (packages) {
            if (selected) { // OK
                packages.add(packageName);
            } else {
                packages.remove(packageName);
            }
            JSONArray arr = new JSONArray();
            for (String p : packages) { // $COMMENT
                arr.put(p);
            }
            settings().arraySettings(R.string.settings_packages, arr);
        }
    }

    public boolean packageSelected(String packageName) {
        if (allPackages) { // All packages mode
            return true;
        }
        synchronized (packages) {
            return packages.contains(packageName);
        }
    }

    public void handleAction(int id, int index) {
        NotificationTools.NotificationID n = notificationByLocalID(id);
        if (null == n || n.deleted) { // Not found
            logger.w("Notification not found:", id);
            return;
        }
        parseTools.sendAction(n, index);
    }

}
