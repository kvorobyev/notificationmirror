package kvj.notification.mirror.app.impl;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.util.Compat;

import java.io.ByteArrayOutputStream;

import kvj.notification.mirror.R;
import kvj.notification.mirror.app.svc.ActionReceiver;
import kvj.notification.mirror.app.svc.DeleteReceiver;

/**
 * Created by kvorobyev on 4/11/15.
 */
public class NotificationTools {

    public static final String EXTRA_LOCAL_ID = "local_id";
    private final NotificationManager notificationManager;

    public void clear(NotificationID existing) {
        if (null == existing) { // Invalid
            return;
        }
        notificationManager.cancel(existing.localID);
        existing.delete();
    }

    public void show(Notification n, NotificationID id) {
        notificationManager.notify(id.localID, n);
    }

    public static class NotificationID {
        public int id;
        public String packageName = "";
        public String tag = null;
        public int localID;
        public String code = "";
        public boolean deleted = false;
        public Notification notification = null;

        @Override
        public String toString() {
            return NotificationTools.keyFromNotification(this);
        }

        public boolean delete() {
            if (deleted) { // Already
                return false;
            }
            notification = null;
            deleted = true;
            return true;
        }
    }

    public static class NotificationHolder extends Pair<NotificationID, Notification> {

        public NotificationHolder(NotificationID first, Notification second) {
            super(first, second);
        }

        public NotificationID id() {
            return first;
        }

        public Notification notification() {
            return second;
        }
    }

    private final Context context;
    private Logger logger = Logger.forInstance(this);

    public NotificationTools(Context context) {
        this.context = context;
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder fn = new Notification.Builder(context);
        fn
                .setContentTitle("NotificationMirror")
                .setContentText("Application is active")
                .setSmallIcon(R.drawable.ic_stat_full)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setPriority(Notification.PRIORITY_MIN);
        notificationManager.notify(0, fn.build());
        notificationManager.cancel(0);
    }

    public Notification deserializeNotification(final NotificationCompat.Builder nb, final JSONObject json, final int localID) {
        Compat.levelAware(21, new Runnable() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                nb.setCategory(json.optString("category", null));
                nb.setVisibility(json.optInt("visibility", Notification.VISIBILITY_PRIVATE));
                nb.setColor(json.optInt("color", Color.BLACK));
            }
        }, null);
        Compat.levelAware(20, new Runnable() {
            @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
            @Override
            public void run() {
                nb.setGroup(json.optString("group", null));
                nb.setSortKey(json.optString("sort", null));
            }
        }, null);
        final Bundle extras = new Bundle();
        Compat.levelAware(19, new Runnable() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                JSONArray actions = json.optJSONArray("actions");
                if (null != actions) { // OK
                    for (int i = 0; i < actions.length(); i++) {
                        JSONObject act = actions.optJSONObject(i);
                        Intent intent = new Intent(context, ActionReceiver.class);
                        intent.putExtra(ActionReceiver.EXTRA_ID, localID);
                        intent.putExtra(ActionReceiver.EXTRA_INDEX, i);
                        PendingIntent pintent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                        nb.addAction(R.drawable.ic_stat_full, act.optString("title", ""), pintent);
                    }
                }
                JSONObject extrasJson = json.optJSONObject("extras");
                if (null == extrasJson) {
                    return;
                }
                for (String key : extraStrings) {
                    if (extrasJson.has(key)) {
                        extras.putCharSequence(key, extrasJson.optString(key));
                    }
                }
                for (String key : extraInts) {
                    if (extrasJson.has(key)) {
                        extras.putInt(key, extrasJson.optInt(key));
                    }
                }
                for (String key : extraBools) {
                    if (extrasJson.has(key)) {
                        extras.putBoolean(key, extrasJson.optBoolean(key));
                    }
                }
                for (String key : extraBitmaps) {
                    if (extrasJson.has(key)) {
                        extras.putParcelable(key, deserializeBitmap(extrasJson.optString(key)));
                    }
                }
                if (extrasJson.has(Notification.EXTRA_TEMPLATE)) { // Try to restore template
                    extras.putString(Notification.EXTRA_TEMPLATE, extrasJson.optString(Notification.EXTRA_TEMPLATE));
                }
                if (extrasJson.has(Notification.EXTRA_TEXT_LINES)) { // Try to restore array
                    JSONArray arr = extrasJson.optJSONArray(Notification.EXTRA_TEXT_LINES);
                    CharSequence[] lines = new CharSequence[arr.length()];
                    NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle(nb);
                    for (int i = 0; i < arr.length(); i++) { // $COMMENT
                        lines[i] = arr.optString(i);
                        style.addLine(lines[i]);
                    }
                    if (extras.containsKey(Notification.EXTRA_TITLE_BIG)) {
                        style.setBigContentTitle(extras.getCharSequence(Notification.EXTRA_TITLE_BIG));
                    }
                    extras.putCharSequenceArray(Notification.EXTRA_TEXT_LINES, lines);
                    nb.setStyle(style);
                }
                nb.setExtras(extras);
            }
        }, null);
        nb.setDefaults(json.optInt("defaults", 0));
        final Bitmap largeIcon = deserializeBitmap(json.optString("largeIcon", null));
        nb.setLargeIcon(largeIcon);
        nb.setLights(json.optInt("ledARGB"), json.optInt("ledOnMS"), json.optInt("ledOffMS"));
        nb.setNumber(json.optInt("number"));
        nb.setPriority(json.optInt("priority", Notification.PRIORITY_DEFAULT));
        nb.setTicker(json.optString("tickerText"));
        nb.setWhen(json.optLong("when"));
        nb.setContentTitle(extras.getCharSequence(Notification.EXTRA_TITLE, json.optString("package")));
        nb.setContentText(extras.getCharSequence(Notification.EXTRA_TEXT, json.optString("tickerText")));
        if (extras.containsKey(Notification.EXTRA_SUB_TEXT)) {
            nb.setSubText(extras.getCharSequence(Notification.EXTRA_SUB_TEXT));
        }
        if (extras.containsKey(Notification.EXTRA_INFO_TEXT)) {
            nb.setContentInfo(extras.getCharSequence(Notification.EXTRA_INFO_TEXT));
        }
        if (null == largeIcon) { // Need to load icon
            Bitmap icon = deserializeBitmap(json.optString("iconBitmap"));
            if (null != icon) { // Set rendered
                nb.setLargeIcon(fromLargeIcon(icon));
            }
        } else {
            // Resize
            Compat.levelAware(21, new Runnable() {
                @Override
                public void run() {
                    nb.setLargeIcon(largeIcon);
                }
            }, new Runnable() {
                @Override
                public void run() {
                    int size = (int) (ICON_SIZE_RENDER * context.getResources().getDisplayMetrics().density);
                    nb.setLargeIcon(Bitmap.createScaledBitmap(largeIcon, size, size, false));
                }
            });
        }
        Notification n  = nb.build();
        logger.d("New Notification", n.flags, json.optInt("flags"));
        n.flags = json.optInt("flags", 0);
        n.flags &= ~(Notification.FLAG_FOREGROUND_SERVICE);
        return n;
    }

    private static String[] extraStrings = {
            Notification.EXTRA_BIG_TEXT,
            Notification.EXTRA_INFO_TEXT,
            Notification.EXTRA_SUB_TEXT,
            Notification.EXTRA_SUMMARY_TEXT,
            Notification.EXTRA_TEMPLATE,
            Notification.EXTRA_TEXT,
            Notification.EXTRA_TITLE,
            Notification.EXTRA_TITLE_BIG};
    private static String[] extraInts = {
            Notification.EXTRA_COMPACT_ACTIONS,
            Notification.EXTRA_PROGRESS_MAX,
            Notification.EXTRA_PROGRESS
    };
    private static String[] extraBools = {
            Notification.EXTRA_PROGRESS_INDETERMINATE,
            Notification.EXTRA_SHOW_CHRONOMETER,
            Notification.EXTRA_SHOW_WHEN
    };
    private static String[] extraBitmaps = {
            Notification.EXTRA_LARGE_ICON,
            Notification.EXTRA_LARGE_ICON_BIG,
            Notification.EXTRA_PICTURE
    };


    private void serializeNotification(final Notification n, final JSONObject json) {
        Compat.levelAware(21, new Runnable() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                try {
                    json.put("category", n.category);
                    json.put("visibility", n.visibility);
                    json.put("color", n.color);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, null);
        Compat.levelAware(20, new Runnable() {
            @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
            @Override
            public void run() {
                try {
                    json.put("group", n.getGroup());
                    json.put("sort", n.getSortKey());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, null);
        Compat.levelAware(19, new Runnable() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                if (null != n.actions) { // Have actions
                    JSONArray arr = new JSONArray();
                    try {
                        for (Notification.Action a : n.actions) { // $COMMENT
                            JSONObject act = new JSONObject();
                            act.put("title", a.title.toString());
                            arr.put(act);
                        }
                        json.put("actions", arr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (null == n.extras) {
                    return;
                }
                try {
                    JSONObject extras = new JSONObject();
                    for (String key : extraStrings){
                        if (n.extras.containsKey(key)) {
                            CharSequence cs = n.extras.getCharSequence(key);
                            if (null != cs) {
                                extras.put(key, cs.toString());
                            }
                        }
                    }
                    for (String key : extraInts){
                        if (n.extras.containsKey(key)) {
                            extras.put(key, n.extras.getInt(key));
                        }
                    }
                    for (String key : extraBools){
                        if (n.extras.containsKey(key)) {
                            extras.put(key, n.extras.getBoolean(key));
                        }
                    }
                    for (String key : extraBitmaps){
                        if (n.extras.containsKey(key) && null != n.extras.getParcelable(key)) {
                            // extras.put(key, serializeBitmap((Bitmap) n.extras.getParcelable(key)));
                        }
                    }
                    extras.put(Notification.EXTRA_TEMPLATE, n.extras.getString(Notification.EXTRA_TEMPLATE));
                    if (n.extras.containsKey(Notification.EXTRA_TEXT_LINES)) { // Array
                        CharSequence[] lines = n.extras.getCharSequenceArray(Notification.EXTRA_TEXT_LINES);
                        JSONArray arr = new JSONArray();
                        for (CharSequence line : lines) { // $COMMENT
                            arr.put(line == null? line: line.toString());
                        }
                        extras.put(Notification.EXTRA_TEXT_LINES, arr);
                    }
                    json.put("extras", extras);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, null);
        try {
            json.put("defaults", n.defaults);
            json.put("flags", n.flags);
            json.put("icon", n.icon);
            json.put("iconLevel", n.iconLevel);
            // json.put("largeIcon", serializeBitmap(n.largeIcon));
            json.put("ledARGB", n.ledARGB);
            json.put("ledOffMS", n.ledOffMS);
            json.put("ledOnMS", n.ledOnMS);
            json.put("number", n.number);
            json.put("priority", n.priority);
            json.put("tickerText", n.tickerText != null? n.tickerText.toString(): null);
            json.put("when", n.when);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //    private <T extends Parcelable> T deserializeParcelable(String base64, Parcelable.Creator<T> creator) {
//        if (TextUtils.isEmpty(base64)) { // No data
//            return null;
//        }
//        Parcel p = Parcel.obtain();
//        try {
//            byte[] data = Base64.decode(base64, Base64.NO_WRAP);
//            logger.d("Bitmap data size2:", data.length);
//            p.unmarshall(data, 0, data.length);
//            p.setDataPosition(0);
//            return creator.createFromParcel(p);
//        } catch (Exception e) {
//            logger.e(e, "Deserialize failed:", base64.length());
//        }
//        return null;
//    }
//
    private Bitmap deserializeBitmap(String base64) {
        if (TextUtils.isEmpty(base64)) { // No data
            return null;
        }
        try {
            byte[] data = Base64.decode(base64, Base64.NO_WRAP);
            return BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (Exception e) {
            logger.e(e, "Deserialize failed:", base64.length());
        }
        return null;
    }

    private String serializeBitmap(Bitmap bitmap) {
        if (null == bitmap) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (!bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream)) {
            return null;
        }
        return Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP);
    }
//
//    private String serializeParcelable(Parcelable parcelable) {
//        if (null == parcelable) {
//            return null;
//        }
//        Parcel p = Parcel.obtain();
//        parcelable.writeToParcel(p, 0);
//        byte[] data = p.marshall();
//        logger.d("Bitmap data size1:", data.length);
//        return Base64.encodeToString(data, Base64.NO_WRAP);
//    }

    public JSONObject serializeSimple(NotificationID id) {
        final JSONObject json = new JSONObject();
        try {
            json.put("id", id.id);
            json.put("package", id.packageName);
            json.put("tag", id.tag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public JSONObject serialize(final StatusBarNotification sbn, boolean remove) {
        final JSONObject json = new JSONObject();
        try {
            json.put("id", sbn.getId());
            json.put("package", sbn.getPackageName());
            json.put("tag", sbn.getTag());
            Compat.levelAware(20, new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
                @Override
                public void run() {
                    try {
                        json.put("key", sbn.getKey());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, null);
            if (remove) { // No other data
                return json;
            }
            Compat.levelAware(21, new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    try {
                        json.put("_group", sbn.getGroupKey());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, null);
//            json.put("iconBitmap", serializeBitmap(fromIcon(sbn)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        serializeNotification(sbn.getNotification(), json);
        return json;
    }

    private static final int ICON_SIZE = 64;
    private static final int ICON_SIZE_SMALL = 24;
    private static final int ICON_SIZE_RENDER = 48;
    private static final int DENSITY = 3;

    private Bitmap fromLargeIcon(Bitmap icon) {
        try {
            float density = context.getResources().getDisplayMetrics().density;
            int size = (int) (ICON_SIZE_RENDER * density);
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Paint p = new Paint();
            Drawable circle = context.getResources().getDrawable(R.drawable.notification_icon_bg);
            circle.setBounds(canvas.getClipBounds());
            circle.draw(canvas);
            int iconStart = (int) ((ICON_SIZE_RENDER-ICON_SIZE_SMALL)/2*density);
            int iconSize = (int) (ICON_SIZE_SMALL * density + iconStart);
            canvas.drawBitmap(icon,
                    null,
                    new Rect(iconStart, iconStart, iconSize, iconSize),
                    p);
            return bitmap;
        } catch (Exception e) {
            logger.e(e, "Failed to load icon");
        }
        return null;
    }

    private Bitmap fromIcon(StatusBarNotification sbn) {
        try {
            int size = ICON_SIZE * DENSITY;
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Paint p = new Paint();
//            Drawable circle = context.getResources().getDrawable(R.drawable.notification_icon_bg);
//            circle.setBounds(canvas.getClipBounds());
//            circle.draw(canvas);
            Resources rs = context.getPackageManager().getResourcesForApplication(sbn.getPackageName());
            Bitmap iconBitmap = BitmapFactory.decodeResource(rs, sbn.getNotification().icon);
//            int iconStart = (ICON_SIZE-ICON_SIZE_SMALL)/2*DENSITY;
            if (null == iconBitmap) { // No icon
                return null;
            }
            canvas.drawBitmap(iconBitmap,
                    null,
                    new Rect(0, 0, size, size),
                    p);
            return bitmap;
        } catch (Exception e) {
            logger.e(e, "Failed to load icon");
        }
        return null;
    }

    public static NotificationID idFromNotification(final StatusBarNotification sbn) {
        final NotificationID id = new NotificationID();
        id.id = sbn.getId();
        id.packageName = sbn.getPackageName();
        id.tag = sbn.getTag();
        return id;
    }

    public static NotificationID idFromJSON(JSONObject json, String code) {
        NotificationID id = new NotificationID();
        id.id = json.optInt("id");
        id.packageName = json.optString("package");
        id.tag = json.optString("tag", null);
        id.code = code;
        return id;
    }

    public static String keyFromNotification(NotificationID id) {
        return String.format("%s|%d|%s|%s", id.packageName, id.id, id.tag, id.code);
    }

    public static String keyFromNotification(final StatusBarNotification sbn) {
        return String.format("%s|%d|%s", sbn.getPackageName(), sbn.getId(), sbn.getTag());
    }

    public static boolean isClosable(Notification notification) {
        return ((notification.flags & Notification.FLAG_ONGOING_EVENT) == 0)
                && ((notification.flags & Notification.FLAG_NO_CLEAR) == 0);
    }

    public PendingIntent createDeleteIntent(NotificationID id) {
        Intent intent = new Intent(context, DeleteReceiver.class);
        intent.putExtra(EXTRA_LOCAL_ID, id.localID);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

//    private int[] flags = {Notification.FLAG_AUTO_CANCEL,
//    Notification.FLAG_FOREGROUND_SERVICE,
//    Notification.FLAG_GROUP_SUMMARY,
//    Notification.FLAG_HIGH_PRIORITY,
//    Notification.FLAG_INSISTENT,
//    Notification.FLAG_LOCAL_ONLY,
//    Notification.FLAG_NO_CLEAR,
//    Notification.FLAG_ONGOING_EVENT,
//    Notification.FLAG_ONLY_ALERT_ONCE,
//    Notification.FLAG_SHOW_LIGHTS};
//
//    private void printFlags(Notification n) {
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < flags.length; i++) { // $COMMENT
//            if ((n.flags & flags[i]) != 0) { // Flags set
//                sb.append(" ");
//                sb.append(i);
//            }
//        }
//        logger.d("Flags:", sb);
//    }
}
