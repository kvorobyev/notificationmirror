package kvj.notification.mirror.app.svc;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import org.kvj.bravo7.log.Logger;
import org.kvj.bravo7.ng.App;
import org.kvj.bravo7.util.Compat;

import kvj.notification.mirror.app.NotificationController;
import kvj.notification.mirror.app.impl.NotificationTools;

/**
 * Created by kvorobyev on 4/9/15.
 */
public class NotificationListener2 extends NotificationListenerService {

    Logger logger = Logger.forClass(NotificationListener2.class);
    NotificationController controller = App.controller();
    private NotificationManager notificationManager = null;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        logger.d("Listener created");
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        controller.notificationListener(this, notifications());
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        controller.notificationListener(this, notifications());
        if (sbn.getPackageName().equals(getPackageName())) { // Own notification - skip
            return;
        }
        controller.sendNotification(sbn, false);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        controller.notificationListener(this, notifications());
        if (sbn.getPackageName().equals(getPackageName())) { // Own notification - translate and close
            return;
        }
        controller.sendNotification(sbn, true);
    }

    private void clear(NotificationTools.NotificationID id) {
        logger.d("Clearing own:", id.tag, id.localID);
        notificationManager.cancel(id.tag, id.localID);
    }

    public void cancel(final StatusBarNotification sbn) {
        if (null == sbn) {
            return;
        }
        logger.d("Will clear", controller.notificationTools().keyFromNotification(sbn));
        Compat.levelAware(21, new Runnable() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                cancelNotification(sbn.getKey());
            }
        }, new Runnable() {
            @Override
            public void run() {
                cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
            }
        });
    }

    public StatusBarNotification find(NotificationTools.NotificationID id) {
        for (StatusBarNotification sbn : notifications()) {
            if (sbn.getId() == id.id && sbn.getPackageName().equals(id.packageName)) { // Found
                return sbn;
            }
        }
        return null;
    }

    public StatusBarNotification findLocal(NotificationTools.NotificationID id) {
        for (StatusBarNotification sbn : getActiveNotifications()) {
            if (sbn.getId() == id.localID && sbn.getPackageName().equals(getPackageName())) { // Found
                return sbn;
            }
        }
        return null;
    }

    public StatusBarNotification[] notifications() {
        try {
            StatusBarNotification[] notifications = getActiveNotifications();
            if (null != notifications) { // Fail
                return notifications;
            }
        } catch (Exception e) {
            logger.e(e, "Error getting notifications");
        }
        return new StatusBarNotification[0];
    }
}
